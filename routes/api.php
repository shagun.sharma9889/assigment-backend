<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\ReportsController;
use App\Http\Controllers\API\MailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([ 'middleware' => 'api','prefix' => 'auth'], function ($router) {
    Route::get('/unauthenticated', [AuthController::class, 'unauthenticated']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);   
    Route::post('/admin-user-update/{id}', [AuthController::class, 'adminUserUpdate']);   
    Route::get('/delete-user/{id}', [AuthController::class, 'adminUserdelete']);   

    Route::get('/get-all-profiles', [ProfileController::class, 'getAllProfile']);

    Route::post('/profile-create', [ProfileController::class, 'createProfile']);
    Route::post('/add-profile-report', [ProfileController::class, 'addProfileReport']);
    Route::post('/user-profile/{id}', [ProfileController::class, 'updateProfile']);

    Route::post('/report-create', [ReportsController::class, 'createReport']);
    Route::get('/get-profile-report', [ReportsController::class, 'getProfileReport']);
    Route::post('/report-update/{id}', [ReportsController::class, 'updateReport']);
    Route::get('/report-delete/{id}', [ReportsController::class, 'deleteReport']);

});

