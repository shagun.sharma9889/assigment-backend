<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Mail\NotifyMail;
use Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */

     public function sendResponse($data, $message, $status = 200) 
     {
         $response = [
             'code' => $status,
             'status' => true,
             'data' => $data,
             'message' => $message
         ];
 
         return response()->json($response, $status);
     }

     public function sendError($errorData, $message, $status = 500)
     {
         $response = [];
         $response = [
             'code' => $status,
             'status' => false,
             'message' => $message
         ];
         if (!empty($errorData)) {
             $response['data'] = $errorData;
         }
 
         return response()->json($response, $status);
     }
     


     public function unauthenticated(Request $request) {
        return(response()->json(
            [
                'api_status' => '401',
                'message' => 'UnAuthenticated',
            ], 401));
     }
        
     /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|between:2,100',
            'email' => 'required|email|max:100|unique:users',
            'password' => 'required|string|min:6',
        ]);
        if($validator->fails()){
            return $this->sendError($validator->errors(), 'Validation Error', 422);
        }
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' =>  isset($request->last_name) ? $request->last_name: '',
            'dob' =>  isset($request->dob) ? $request->dob: '',
            'gender' => isset($request->gender) ? $request->gender: '',
            'email' => $request->email,
            'role_id' => 2,
            'password' => Hash::make($request->password),
            'created_by' => isset($request->created_by) ? $request->created_by: '',

        ]);

        return $this->sendResponse($user, 'user registered successfully', 200);        

    }
    public function login(Request $request){

        $input = $request->only('email', 'password');

        $validator = Validator::make($input, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors(), 'Validation Error', 422);
        }

        try {
            // this authenticates the user details with the database and generates a token
            if (! $token = JWTAuth::attempt($input)) {
                return $this->sendError([], "invalid login credentials", 400);
            }
        } catch (JWTException $e) {
            return $this->sendError([], $e->getMessage(), 500);
        }
        
        $user = JWTAuth::setToken($token)->toUser();
        $success = [
            'token' => $token
        ];
        $success['user'] = $user;

        return $this->sendResponse($success, 'successful login', 200);
    }
    

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        $user = auth()->user();
        auth()->logout();
        return $this->sendResponse($user, 'User successfully signed out', 200);        

    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        // return response()->json(auth()->user());
        $user = auth()->user();
        return $this->sendResponse($user, 'User retrive successful', 200);

    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
       
        $response['access_token'] = $token;
        $response['token_type'] = 'bearer';
        $response['expires_in'] = auth()->factory()->getTTL() * 60;
        $response['user'] = auth()->user();
        return $this->sendResponse($response, 'Token refresh successful', 200);

    }

    protected function adminUserUpdate(Request $request, $id){

    
        $user = User::find($id);   
        if(!empty($user)){
            $user->first_name = isset($request->first_name) ? $request->first_name : $user['first_name'];
            $user->last_name =  isset($request->last_name) ? $request->last_name : $user['last_name'];
            $user->dob =  isset($request->dob) ? $request->dob : $user['dob'];
            $user->gender = isset($request->gender) ? $request->gender : $user['gender'];
            $user->status =  isset($request->status) ? $request->status : $user['status'];
            $user->update();
            return $this->sendResponse($user, 'Admin update profile successfully', 200);  
        } else {
            return $this->sendError([], 'Data not found', 404);
        }

    }

    public function adminUserdelete($id)
    {
        $user = User::find($id);   
        if(!empty($user)){
            $user->delete();
            return $this->sendResponse($user, 'Profile deleted successfully', 200);  
        } else {
            return $this->sendError([], 'Data not found', 404);
        }
    }
    
}
