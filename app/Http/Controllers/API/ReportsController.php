<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Mail\NotifyMail;
use Mail;
use App\Models\User;
use App\Models\Profile;
use App\Models\Report;
use Validator;

class ReportsController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function createReport(Request $request)
   {
    // Validate the request
    $validator = Validator::make($request->all(), [
        'title' => 'required',
        'description' => 'required',
    ]);

    if ($validator->fails()) {
        return $this->sendError($validator->errors(), 'Validation Error', 422);
    }



    // Create the profile
    $report = Report::create([
        'title' => $request->title,
        'description' => $request->description,
        'user_id'=> Auth::user()->id 
    ]);

    
    
    return $this->sendResponse($report, 'Report created successfully', 200);        
}

    public function updateReport(Request $request,$id){

        $report = Report::find($id);   
        if(!empty($report)){
            $report->title = isset($request->title) ? $request->title : $report['title'];
            $report->description =  isset($request->description) ? $request->description : $report['description'];;
            $report->update();
            return $this->sendResponse($report, 'Report updated successfully', 200);  
        } else {
            return $this->sendError([], 'Data not found', 404);
        }

    }
    
    public function deleteReport($id)
    {
        $report = Report::find($id);   
        if(!empty($report)){
            $report->delete();
            return $this->sendResponse($Report, 'Profile deleted successfully', 200);  
        } else {
            return $this->sendError([], 'Data not found', 404);
        }
    }


    public function getProfileReport(){
        $profilereport = Report::with('getporstprofiles')->get();
        return $this->sendResponse($profilereport, 'Profile and report retrive successfully', 200); 
    }
public function sendResponse($data, $message, $status = 200) 
{
    $response = [
        'code' => $status,
        'status' => true,
        'data' => $data,
        'message' => $message
    ];

    return response()->json($response, $status);
}

public function sendError($errorData, $message, $status = 500)
{
    $response = [];
    $response = [
        'code' => $status,
        'status' => false,
        'message' => $message
    ];
    if (!empty($errorData)) {
        $response['data'] = $errorData;
    }

    return response()->json($response, $status);
}
}
