<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\NotifyMail;
use Mail;
use App\Models\User;
use App\Models\Profile;
use App\Models\ProfileReport;
use App\Models\Report;
use Validator;
use Dompdf\Dompdf;
use Dompdf\Options;


class ProfileController extends Controller
{
      
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function getAllProfile()
    {
        $loggedInUserId = Auth::id();
        $users = User::where('id', '!=', $loggedInUserId)->get();

        if(!empty($users)){
            return $this->sendResponse($users, 'Profiles retrive successfully', 200);  
        } else {
            return $this->sendError([], 'Data not found', 404);
        }

    }
    
    public function createProfile(Request $request)
   {
    // Validate the request
    $validator = Validator::make($request->all(), [
        'first_name' => 'required',
        'dob' => 'required',
        'gender' => 'required',
        'email' => 'required|email|max:100|unique:users',

    ]);

    if ($validator->fails()) {
        return $this->sendError($validator->errors(), 'Validation Error', 422);
    }

    // Check if the authenticated user is an admin
    if (Auth::user()->role_id !== '1') {
        return $this->sendError(['error' => 'Unauthorized'], 'Validation Error', 401);

    }

    // Create the profile
    $profile = User::create([
        'first_name' => $request->first_name,
        'last_name' =>  isset($request->last_name) ? $request->last_name: '',
        'dob' =>  isset($request->dob) ? $request->dob: '',
        'email' => $request->email,
        'gender' => isset($request->gender) ? $request->gender: '',
        'password'=> Hash::make('12345678'),
        'role_id' => 2,
        'created_by'=> Auth::user()->id 
    ]);
    $data = array('name'=>$profile['name'],'email'=>$profile['email'],'password'=> '12345678');

//     Mail::send('email-templates/send-details',['data'=> $data], function($message)  use ($data)  {
//        $message->to($data['email'])->subject('User credenatils');
//        $message->from('admin@gmail.com','Admin');
//    });
    return $this->sendResponse($profile, 'Profile created successfully', 200);        
}


    public function updateProfile(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name'=> 'required',
        ]);
    
        if ($validator->fails()) {
            return $this->sendError($validator->errors(), 'Validation Error', 422);
        }
        $user = User::find($id);   
        if(!empty($user)){
            $user->first_name = isset($request->first_name) ? $request->first_name : $user['first_name'];
            $user->last_name =  isset($request->last_name) ? $request->last_name : $user['last_name'];
            $user->dob =  isset($request->dob) ? $request->dob : $user['dob'];
            $user->gender = isset($request->gender) ? $request->gender : $user['gender'];
            $user->update();
            return $this->sendResponse($user, 'Profile updated successfully', 200);  
        } else {
            return $this->sendError($validator->errors(), 'Data not found', 404);
        }
       
 

    }

    public function addProfileReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'report_id' => 'required',
        ]);
    
        if ($validator->fails()) {
            return $this->sendError($validator->errors(), 'Validation Error', 422);
        }

        $profileReport = ProfileReport::create([
            'user_id' => $request->user_id,
            'report_id' => $request->report_id,
        ]);
        $user = User::find($request->user_id);
        $report = Report::find($request->report_id);
                
        $data = array('name'=>$user['name'],'email'=>$user['email'],'report_name'=> $report['title'],'report_description'=> $report['description']);

    //        Mail::send('email-templates/send-report',['data'=> $data], function($message)  use ($data)  {
    //        $message->to( $data['email'])->subject('Report Details');
    //        $message->from('admin@gmail.com','Admin');
    //    });
        return $this->sendResponse($profileReport, 'Profile and report assinged successfully', 200); 

    }
   
    


        public function sendResponse($data, $message, $status = 200) 
        {
            $response = [
                'code' => $status,
                'status' => true,
                'data' => $data,
                'message' => $message
            ];

            return response()->json($response, $status);
        }

        public function sendError($errorData, $message, $status = 500)
        {
            $response = [];
            $response = [
                'code' => $status,
                'status' => false,
                'message' => $message
            ];
            if (!empty($errorData)) {
                $response['data'] = $errorData;
            }

            return response()->json($response, $status);
        }


}
