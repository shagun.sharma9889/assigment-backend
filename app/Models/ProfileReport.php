<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileReport extends Model
{
    use HasFactory;
    protected $table = 'report_user';
    protected $fillable = [
        'user_id',
        'report_id',
    ];
}
