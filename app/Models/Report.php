<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;
    protected $table = 'reports';
    protected $fillable = [
        'title',
        'description',
        'user_id'
    ];

    public function getprofiles()
    {
        return $this->belongsToMany(Profile::class);
    }

    public function getporstprofiles()
    {
        return $this->belongsToMany(User::class)->withPivot('user_id', 'report_id');
        // return $this->belongsTo(User::class, 'user_id');

    }
}
