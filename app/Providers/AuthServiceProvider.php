<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('profile-create', function (User $user) {
            return $user->role_id === 1; // Assuming role_id 1 is for admin
        });
        Gate::define('/admin-user-update/{id}', function (User $user) {
            return $user->role_id === 1; // Assuming role_id 1 is for admin
        });
        Gate::define('/delete-user/{id}', function (User $user) {
            return $user->role_id === 1; // Assuming role_id 1 is for admin
        });
        Gate::define('/get-all-profiles', function (User $user) {
            return $user->role_id === 1; // Assuming role_id 1 is for admin
        });
    
    }
}
